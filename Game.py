from Board import Board
import pygame
from Endgame import save_result


# Класс с интерфейсом и его работой
class Game:
    def __init__(self, board, size=800, num=8):
        self.chosen_cords = (0, 0)
        self.board = board
        self.size = size
        self.one = int(size / num)
        self.num = num
        self.chosen = None
        self.goes = 1
        self.END = False
        self.part = 50
        self.left = 100
        self.sides = (None, 'Белые', 'Черные')
        self.winner = 0
        self.log = []  # Сюда записываются все ходы и результат партии

    # Стартовый экран
    def render_start_screen(self, scr):
        scr.fill((255, 255, 255))
        pygame.draw.rect(scr, (0, 0, 0), (350, 350, 400, 150), width=5)
        screen.blit(pygame.font.Font(None, 75).render("Начать игру", True, (0, 0, 0)), (400, 400))
        screen.blit(pygame.font.Font(None, 100).render("Квантовые шахматы", True, (0, 0, 0)), (200, 100))

    # Нажатие по стартовому экрану
    def start_click(self, pos):
        if 350 <= pos[0] <= 750 and 350 <= pos[1] <= 500:
            return True
        else:
            return False

    # Отрисовка всего
    def render(self, scr):
        scr.fill((255, 255, 255))
        black = pygame.Color(0, 0, 0)
        pygame.draw.rect(scr, (100, 100, 100), (0, 0, self.size, self.size))
        pygame.draw.rect(scr, (0, 0, 0), (830, 100, 100, 100), width=5)
        pygame.draw.rect(scr, (0, 0, 0), (970, 100, 100, 100), width=5)
        pygame.draw.rect(scr, (0, 0, 0), (830, 300, 240, 100), width=5)
        pygame.draw.rect(scr, (0, 0, 0), (830, 450, 240, 100), width=5)
        size = int(self.size / self.num)
        start = 0
        # Поле
        for i in range(self.num):
            cord = start
            while cord < self.size:
                pygame.draw.rect(scr, (255, 255, 255),
                                 (cord, self.size - size * (i + 1), size, size))
                cord += size * 2
            if not start:
                start = size
            else:
                start = 0
        # Если выбрана фигура то показывает ее возможные ходы
        if self.chosen:
            cords = self.get_cords(self.chosen.cords)
            cords = cords[0] + 50, cords[1] + 50
            pygame.draw.circle(scr, (100, 100, 255), cords, 50)
            possible_moves = board.possible_moves(self.chosen)
            possible_hits = board.possible_hits(self.chosen)
            for i in possible_moves:
                cords = self.get_cords(i)
                cords = cords[0] + 50, cords[1] + 50
                try:
                    if not board(i):
                        pygame.draw.circle(scr, (255, 255, 130), cords, 50)
                except Exception:
                    pass

            for i in possible_hits:
                cords = self.get_cords(i)
                cords = cords[0] + 50, cords[1] + 50
                try:
                    if board(i) and board(i).side != self.chosen.side:
                        pygame.draw.circle(scr, (255, 50, 50), cords, 50)
                except Exception:
                    pass
        # Отрисовка фигур
        for row in self.board():
            for column in row:
                if column:
                    pos = self.get_cords(column.cords)
                    color = 0, 0, 0
                    screen.blit(pygame.font.Font(None, 25).render(str(column.part) + '%', True, color), pos)
                    pos = pos[0] + 5, pos[1] + 10
                    scr.blit(self.load_image(column()), pos)
        pygame.draw.line(scr, black, (self.size, 0), (self.size, self.size), 3)
        screen.blit(pygame.font.Font(None, 35).render(f"Ход:{self.left}% {self.sides[self.goes]}", True, (0, 0, 0)),
                    (810, 25))
        screen.blit(pygame.font.Font(None, 150).render("-", True, (0, 0, 0)), (860, 95))
        screen.blit(pygame.font.Font(None, 150).render("+", True, (0, 0, 0)), (990, 90))
        screen.blit(pygame.font.Font(None, 75).render(f"Часть:{self.part}%", True, (0, 0, 0)), (810, 225))
        screen.blit(pygame.font.Font(None, 40).render("Закончить ход", True, (0, 0, 0)), (845, 335))
        screen.blit(pygame.font.Font(None, 55).render("Конец игры", True, (0, 0, 0)), (835, 480))
        if self.END:
            screen.blit(pygame.font.Font(None, 35).render(self.msg,
                                                          True, (0, 0, 0)), (840, 580))

    # Загрузка изображений фигур по их названиям
    def load_image(self, name):
        return pygame.transform.scale(pygame.image.load(f'textures/{name}.png').convert_alpha(),
                                      (90, 90))

    # Нажатие ЛКМ и ПКМ
    def get_click(self, mouse_pos, button):
        self.button = button
        if not self.END:
            # Клики по доске
            if mouse_pos[0] <= 800:
                cell = self.get_cell(mouse_pos)
                self.on_click(cell)
            # Кнопка Закончить ход
            elif 830 <= mouse_pos[0] <= 1070 and 300 <= mouse_pos[1] <= 400:
                self.goes *= -1
                self.left = 100
            # Кнопка Конец игры
            elif 830 <= mouse_pos[0] <= 1070 and 450 <= mouse_pos[1] <= 550:
                if board.get_percents_of_kings()[0] < 10 or board.get_percents_of_kings()[1] < 10:
                    if board.get_percents_of_kings()[0] > board.get_percents_of_kings()[1]:
                        self.msg = 'Победили белые'
                        self.log.append(self.msg)
                        self.winner = 1
                    else:
                        self.msg = 'Победили черные'
                        self.log.append(self.msg)
                        self.winner = -1
                elif self.goes == -1:
                    self.msg = 'Победили белые'
                    self.log.append(self.msg)
                    self.winner = 1
                elif self.goes == 1:
                    self.msg = 'Победили черные'
                    self.log.append(self.msg)
                    self.winner = -1
                self.END = True
            # Кнопки + и -
            elif 830 <= mouse_pos[0] <= 930 and 100 <= mouse_pos[1] <= 200:
                self.part = max(1, self.part - 1)
            elif 970 <= mouse_pos[0] <= 1070 and 100 <= mouse_pos[1] <= 200:
                self.part = min(100, self.part + 1)

    # Возвращает координаты клетки на которую кликнули
    def get_cell(self, pos):
        return 7 - int(pos[1] / self.one), int(pos[0] / self.one)

    # Возвращает положение клетки по ее координатам
    def get_cords(self, cell):
        return cell[1] * self.one, (7 - cell[0]) * self.one

    # Колесо мыши вперед
    def wheel_up(self):
        self.part = min(100, self.part + 5)

    # Колесо мыши назад
    def wheel_down(self):
        self.part = max(1, self.part - 5)

    # ПКМ и ЛКМ
    def on_click(self, cell):
        percents_of_kings = board.get_percents_of_kings()
        end_dictionary_message = {
            percents_of_kings[1]: ('Победили белые', 1),
            percents_of_kings[0]: ('Победили чёрные', -1)
        }
        for key in end_dictionary_message.keys():
            if not key:
                self.msg = end_dictionary_message[key][0]
                self.winner = end_dictionary_message[key][1]
                self.log.append(self.msg)
                self.END = True
                break
        # Выбор фигуры
        if not self.chosen and board(cell) and board(cell).side == self.goes:
            self.chosen = board(cell)
            self.chosen_cords = self.chosen.cords
        # Нажатие по пустой клетке
        elif not self.chosen:
            board(cell)
        # Выбор другой фигуры
        elif self.chosen and board(cell) and self.chosen.side == board(cell).side:
            self.chosen = board(cell)
            self.chosen_cords = self.chosen.cords
        # Ход
        elif not board(cell) and self.button == 'left':
            if self.left >= self.chosen.part:
                self.chosen_cords = self.chosen.cords
                move = board.move(self.chosen, cell)
                if move == 'ok':
                    self.log.append(f'Ход {self.chosen()} {self.chosen.part}% '
                                    f'{self.chosen_cords} - {self.chosen.cords}')
                    self.left -= self.chosen.part
                    self.chosen = None
        # Взятие
        elif self.chosen and board(cell) and self.chosen.side != board(cell).side and self.button == 'left':
            if self.left >= self.chosen.part:
                self.chosen_cords = self.chosen.cords[:]
                reduce = self.chosen.part
                aim = board(cell)(), board(cell).part
                hit = board.hit(self.chosen, board(cell))
                if hit == 'ok':
                    self.log.append(f'Взятие {self.chosen()} {self.chosen.part}% - {aim[0]} {aim[1]}% '
                                    f'{self.chosen_cords} - {self.chosen.cords}')
                    self.left -= reduce
                    self.chosen = None
        # Квантовый ход
        elif not board(cell) and self.button == 'right':
            if self.left >= self.part:
                self.chosen_cords = self.chosen.cords
                move = board.q_move(self.chosen, cell, self.part)
                if move == 'ok':
                    self.log.append(f'Квантовый ход {self.chosen()} {self.part}% '
                                    f'{self.chosen_cords} - {cell}')
                    self.chosen = None
                    self.left -= self.part
        # Квантовое взятие
        elif self.chosen and board(cell) and self.chosen.side != board(cell).side and self.button == 'right':
            if self.left >= self.part:
                self.chosen_cords = self.chosen.cords
                aim = board(cell)(), board(cell).part
                hit = board.q_hit(self.chosen, board(cell), self.part)
                if hit == 'ok':
                    self.log.append(f'Квантовое взятие {self.chosen()} {self.part}% - {aim[0]} {aim[1]}% '
                                    f'{self.chosen_cords} - {cell}')
                    self.chosen = None
                    self.left -= self.part
        # Проверка окончания хода
        if self.left <= 0:
            self.goes *= -1
            self.left = 100

    def get_log(self):
        return self.winner, self.log



if __name__ == '__main__':
    board = Board()
    board.set_default()
    pygame.init()
    size, num = 800, 8
    pygame.display.set_caption('Шахматы с заменителем квантовости')
    screen = pygame.display.set_mode((size + 300, size))
    fps = 60
    clock = pygame.time.Clock()
    game = Game(board)
    running = True
    started = False
    # Игровой цикл
    while running:
        if started:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1:
                        game.get_click(event.pos, 'left')
                    elif event.button == 3:
                        game.get_click(event.pos, 'right')
                    elif event.button == 4:
                        game.wheel_up()
                    elif event.button == 5:
                        game.wheel_down()
            game.render(screen)
        else:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if game.start_click(event.pos):
                        started = True
            game.render_start_screen(screen)
        clock.tick(fps)
        pygame.display.flip()
    result = game.get_log()
    if len(result) > 0:
        save_result(*result)
    pygame.quit()