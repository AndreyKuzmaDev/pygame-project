import time


def save_result(side, message):
    sides = 'none', 'white', 'black'
    date = list(map(str, time.localtime()))[:-3]
    file = open(f'results/{sides[side]}{"-".join(date)}.txt', 'w')
    file.write('\n'.join(message))
