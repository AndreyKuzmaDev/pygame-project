from Figures import Pawn, Rook, Knight, Bishop, Queen, King


# Класс доски
class Board:
    def __init__(self):
        self.board = [[None for _ in range(8)] for __ in range(8)]
        self.types = {'p': Pawn, 'r': Rook, 'k': Knight, 'b': Bishop, 'q': Queen, 'Kg': King}

    # Возвращает положение фигур на доске, или содержимое отдельного поля
    def __call__(self, cords=None):
        if not cords:
            return self.board

        return self.board[cords[0]][cords[1]]

    # Стандартная расстановка
    def set_default(self):
        self.board = [[Rook((0, 0), 1), Knight((0, 1), 1), Bishop((0, 2), 1), King((0, 3), 1), Queen((0, 4), 1),
                       Bishop((0, 5), 1), Knight((0, 6), 1), Rook((0, 7), 1)],
                      [Pawn((1, 0), 1), Pawn((1, 1), 1), Pawn((1, 2), 1), Pawn((1, 3), 1), Pawn((1, 4), 1),
                       Pawn((1, 5), 1), Pawn((1, 6), 1), Pawn((1, 7), 1)],
                      [None for i in range(8)],
                      [None for i in range(8)],
                      [None for i in range(8)],
                      [None for i in range(8)],
                      [Pawn((6, 0), -1), Pawn((6, 1), -1), Pawn((6, 2), -1), Pawn((6, 3), -1), Pawn((6, 4), -1),
                       Pawn((6, 5), -1), Pawn((6, 6), -1), Pawn((6, 7), -1)],
                      [Rook((7, 0), -1), Knight((7, 1), -1), Bishop((7, 2), -1), King((7, 3), -1), Queen((7, 4), -1),
                       Bishop((7, 5), -1), Knight((7, 6), -1), Rook((7, 7), -1)]]

    # Добавление новой фигуры на доску
    def add(self, new_figure):
        if self.board[new_figure.cords[0]][new_figure.cords[1]]:
            return 'cell taken'

        self.board[new_figure.cords[0]][new_figure.cords[1]] = new_figure
        return 'ok'

    # Проверка возможности хода фигуры
    def try_move(self, figure, cell):
        start = figure.cords
        if not isinstance(figure, Knight):
            changes = cell[0] - start[0], cell[1] - start[1]
            step = []
            try:
                step.append(changes[0] // abs(changes[0]))
            except Exception:
                step.append(0)
            try:
                step.append(changes[1] // abs(changes[1]))
            except Exception:
                step.append(0)
            times = max(abs(changes[0]), abs(changes[1]))
            if figure.try_move(cell) == 'ok':
                for i in range(1, times):
                    try:
                        next = start[0] + step[0] * i, start[1] + step[1] * i
                        if self.board[next[0]][next[1]]:
                            return 'way blocked'
                        if next[0] not in range(8) or next[1] not in range(8):
                            raise Exception
                    except Exception:
                        return 'out of board'

            return figure.try_move(cell)
        try:
            if self.board[cell[0]][cell[1]]:
                return 'way blocked'
            elif cell[0] not in range(8) or cell[1] not in range(8):
                raise Exception
        except Exception:
            return 'out of board'

        return figure.try_move(cell)

    # Возвращает все возможные ходы фигуры
    def possible_moves(self, figure):
        res = []
        if isinstance(figure, Pawn) and not figure.moved:
            moves = figure.start_moves
        else:
            moves = figure.moves
        for i in moves:
            new = figure.cords[0] + i[0] * figure.side, figure.cords[1] + i[1]
            if self.try_move(figure, new) == 'ok':
                res.append(new)
        return res

    # Перемещение фигуры
    def move(self, figure, cell):
        start = figure.cords
        if self.try_move(figure, cell) == 'ok':
            self.board[start[0]][start[1]], self.board[cell[0]][cell[1]] = self.board[cell[0]][cell[1]], \
                                                                           self.board[start[0]][start[1]]
            figure.move(cell)
            return 'ok'
        return self.try_move(figure, cell)

    # Квантовый ход
    def q_move(self, figure, cell, part):
        if self.try_move(figure, cell) == 'ok' and part < figure.part:
            figure.is_hit(part)
            self.add(self.types[figure.sym](cell, figure.side, part))
            self.board[cell[0]][cell[1]].moved = True
            return 'ok'
        elif part >= figure.part:
            return 'too big part'
        return self.try_move(figure, cell)

    # Попытка взятия
    def try_hit(self, figure, aim):
        cell = aim.cords
        start = figure.cords
        if not isinstance(figure, (Knight, Pawn)):
            changes = cell[0] - start[0], cell[1] - start[1]
            step = []
            try:
                step.append(changes[0] // abs(changes[0]))
            except Exception:
                step.append(0)
            try:
                step.append(changes[1] // abs(changes[1]))
            except Exception:
                step.append(0)
            times = max(abs(changes[0]), abs(changes[1]))
            if figure.try_move(cell) == 'ok':
                for i in range(1, times):
                    try:
                        next = start[0] + step[0] * i, start[1] + step[1] * i
                        if self.board[next[0]][next[1]]:
                            return 'way blocked'
                        if next[0] not in range(8) or next[1] not in range(8):
                            raise Exception
                    except Exception:
                        return 'out of board'
            else:
                return figure.try_move(cell)
            return figure.try_hit(aim)
        elif figure.sym == 'k':
            try:
                if cell[0] not in range(8) or cell[1] not in range(8):
                    raise Exception
            except Exception:
                return 'out of board'
            return figure.try_hit(aim)
        elif isinstance(figure, Pawn):
            changes = cell[0] - start[0], cell[1] - start[1]
            step = []
            try:
                step.append(changes[0] // abs(changes[0]))
            except Exception:
                step.append(0)
            try:
                step.append(changes[1] // abs(changes[1]))
            except Exception:
                step.append(0)
            return figure.try_hit(aim)

    # Возвращает координаты всех фигуры которые может взять выбранная фигура
    def possible_hits(self, figure):
        res = []
        for i in figure.hits:
            new = figure.cords[0] + i[0] * figure.side, figure.cords[1] + i[1]
            try:
                if self.board[new[0]][new[1]] and self.try_hit(figure, self(new)) == 'ok':
                    res.append(new)
            except Exception:
                continue
        return res

    # Взятие
    def hit(self, figure, aim):
        start = figure.cords
        cell = aim.cords
        if self.try_hit(figure, aim) == 'ok':
            if figure.part >= aim.part:
                self.board[start[0]][start[1]], self.board[cell[0]][cell[1]] = None, self.board[start[0]][start[1]]
            else:
                self.board[start[0]][start[1]] = None
            return figure.hit(aim)

    # Квантовое взятие
    def q_hit(self, figure, aim, part):
        start = figure.cords
        cell = aim.cords
        if self.try_hit(figure, aim) == 'ok' and part < figure.part:
            if part >= aim.part:
                self.board[cell[0]][cell[1]] = None
                self.add(self.types[figure.sym](cell, figure.side, part))
                self.board[cell[0]][cell[1]].moved = True
            else:
                self.board[start[0]][start[0]] = None

            return figure.q_hit(aim, part)
        elif part >= figure.part:
            return 'too big part'

        return self.try_hit(figure, aim)

    # Возвращает сумму частей королей белых и черных
    def get_percents_of_kings(self):
        white_king = 0
        black_king = 0
        for i in self.board:
            for j in i:
                if j and isinstance(j, King):
                    if j.side == 1:
                        white_king += j.part
                        continue
                    black_king += j.part

        return white_king, black_king
