# Основной класс фигуры, от которого наследуются классы всех фигур
class Figure:
    def __init__(self, cords, side, part=100):
        self.sym = 'F'
        self.moves = []  # Список ходов
        self.hits = self.moves[:]  # Список взятий, по стандарту совпадает со списком ходов
        self.cords = cords  # Координаты фигуры
        self.moved = False  # Двигалась ли фигура (нужно для первого хода пешки на два поля)
        self.alive = True  # Жива ли фигура
        self.side = side  # Сторона (1 если белые, -1 если черные)
        self.part = part  # Какая это часть (вероятность) фигуры

    def __call__(self, *args, **kwargs):
        if self.alive:
            colors = ' ', 'w', 'b'
            return colors[self.side] + self.sym

    # Проверка возможности хода по списку возможных ходов фигуры
    def try_move(self, new):
        if self.alive:
            possibles = []
            for i in self.moves:
                possibles.append((self.cords[0] + i[0] * self.side, self.cords[1] + i[1] * self.side))
            if new in possibles:
                return 'ok'
            return 'wrong move'

        return 'figure dead'

    # Проверка возможности взятия фигуры по списку
    def try_hit(self, new):
        if self.alive:
            possibles = []
            for i in self.hits:
                possibles.append((self.cords[0] + i[0] * self.side, self.cords[1] + i[1] * self.side))
            if new.cords in possibles:
                return 'ok'
            return 'wrong move'

        return 'figure dead'

    # Передвижение фигуры
    def move(self, new):
        if self.alive:
            if self.try_move(new) == 'ok':
                self.cords = new
                self.moved = True
                return self.try_move(new)
            return 'wrong move'

        return 'figure dead'

    # Взятие
    def hit(self, other):
        if self.alive and other.alive:
            possibles = []
            for i in self.hits:
                possibles.append((self.cords[0] + i[0] * self.side, self.cords[1] + i[1]))
            if other.cords in possibles and other.side != self.side:
                if self.part >= other.part:
                    self.cords = other.cords
                    self.moved = True
                    other.is_hit(self.part)
                else:
                    other.part -= self.part
                    self.part = 0
                return 'ok'
            return 'wrong'
        elif self.alive:
            return 'aim dead'

        return 'figure dead'

    # Взятие этой фигуры
    def is_hit(self, part):
        self.part -= part
        if self.part <= 0:
            self.alive = False

    # Квантовый ход
    def q_move(self, new, part):
        if self.try_move(new) == 'ok':
            self.part -= part
            return 'ok'

        return self.try_move(new)

    # Квантовое взятие
    def q_hit(self, other, part):
        if self.alive and other.alive:
            possibles = []
            for i in self.hits:
                possibles.append((self.cords[0] + i[0] * self.side, self.cords[1] + i[1]))
            if other.cords in possibles and other.side != self.side:
                self.is_hit(part)
                other.is_hit(part)
                return 'ok'
            return 'wrong'
        elif self.alive:
            return 'aim dead'

        return 'figure dead'


# Класс пешки
class Pawn(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'p'
        self.moved = False
        self.start_moves = [(1, 0), (2, 0)]
        self.moves = [(1, 0)]
        self.hits = [(1, 1), (1, -1)]

    def try_move(self, new):
        if not self.moved:
            moves = self.start_moves
        else:
            moves = self.moves
        if self.alive:
            possibles = []
            for i in moves:
                possibles.append((self.cords[0] + i[0] * self.side, self.cords[1] + i[1] * self.side))
            if new in possibles:
                return 'ok'
            return 'wrong move'
        return 'figure dead'


# Класс ладьи
class Rook(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'r'
        self.moves = [(1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0),
                      (-1, 0), (-2, 0), (-3, 0), (-4, 0), (-5, 0), (-6, 0), (-7, 0),
                      (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7),
                      (0, -1), (0, -2), (0, -3), (0, -4), (0, -5), (0, -6), (0, -7)]
        self.hits = self.moves[:]


# Класс слона
class Bishop(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'b'
        self.moves = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7),
                      (-1, 1), (-2, 2), (-3, 3), (-4, 4), (-5, 5), (-6, 6), (-7, 7),
                      (1, -1), (2, -2), (3, -3), (4, -4), (5, -5), (6, -6), (7, -7),
                      (-1, -1), (-2, -2), (-3, -3), (-4, -4), (-5, -5), (-6, -6), (-7, -7)]
        self.specials = []
        self.hits = self.moves[:]


# Класс ферзя
class Queen(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'q'
        self.moves = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7),
                      (-1, 1), (-2, 2), (-3, 3), (-4, 4), (-5, 5), (-6, 6), (-7, 7),
                      (1, -1), (2, -2), (3, -3), (4, -4), (5, -5), (6, -6), (7, -7),
                      (-1, -1), (-2, -2), (-3, -3), (-4, -4), (-5, -5), (-6, -6), (-7, -7),
                      (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0), (7, 0),
                      (-1, 0), (-2, 0), (-3, 0), (-4, 0), (-5, 0), (-6, 0), (-7, 0),
                      (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7),
                      (0, -1), (0, -2), (0, -3), (0, -4), (0, -5), (0, -6), (0, -7)]
        self.specials = []
        self.hits = self.moves[:]


# Класс коня
class Knight(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'k'
        self.moves = [(1, 2), (-1, 2), (1, -2), (-1, -2),
                      (2, 1), (-2, 1), (2, -1), (-2, -1)]
        self.specials = []
        self.hits = self.moves[:]


# Класс короля
class King(Figure):
    def __init__(self, cords, side, part=100):
        super().__init__(cords, side, part)
        self.sym = 'Kg'
        self.moves = [(1, 1), (1, 0), (1, -1), (0, 1), (0, -1), (-1, 1), (-1, 0), (-1, -1)]
        self.specials = []
        self.hits = self.moves[:]
